package com.hcl.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		Set<String> set = new TreeSet<String>();
		ArrayList<String> list = new ArrayList<String>();
		System.out.println("Count of Employee from each City: ");

		for (Employee cityName : employees) {
			list.add(cityName.getCity());
			set.add(cityName.getCity());
		}

		for (String s : set) {

			System.out.print(s + "=" + Collections.frequency(list, s) + " ");
		}
	}

	public void monthlySalary(ArrayList<Employee> employees) {

		System.out.println("Monthly Salary of Each Employee Along With Their ID: ");
		for (Employee emp : employees) {

			System.out.print(emp.getId() + "=" + emp.getSalary() / 12 + " ");

		}
	}

}
