package com.hcl.assignment2;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {

		ArrayList<Employee> employeeDetails = new ArrayList<>();


		DataStructureA sortedNames = new DataStructureA();
		DataStructureB cityName = new DataStructureB();
		DataStructureB salary = new DataStructureB();

		try {
			
			
			Employee employee1 = new Employee(1, "AMAN", 20, 1100000, "IT", "DELHI");
			Employee employee2 = new Employee(2, "BOBBY", 22, 500000, "HR", "BOMBAY");
			Employee employee3 = new Employee(3, "ZOE", 20, 750000, "ADMIN", "DELHI");
			Employee employee4 = new Employee(4, "SMITHA", 21, 1000000, "IT", "CHENNAI");
			Employee employee5 = new Employee(5, "SMITHA", 24, 1200000, "HR", "BENGULURU");

			employeeDetails.add(employee1);
			employeeDetails.add(employee2);
			employeeDetails.add(employee3);
			employeeDetails.add(employee4);
			employeeDetails.add(employee5);


			for (Employee employee : employeeDetails) {
				if (employee.getId() <= 0 || employee.getName() == null || employee.getAge() <= 0
						|| employee.getSalary() <= 0 || employee.getDepartment() == null
						|| employee.getCity() == null) {
					throw new IllegalArgumentException();
				}
			}

			System.out.println("All the Details of Employees in the Organization: ");
			System.out.println(employeeDetails);
			System.out.println("\n");

			sortedNames.sortingNames(employeeDetails);

			System.out.println("\n");

			cityName.cityNameCount(employeeDetails);

			System.out.println("\n");

			salary.monthlySalary(employeeDetails);

		} catch (IllegalArgumentException i) {
			System.err.println("Exception Occured because Parameters Cannot be Zero or Null ");
			i.printStackTrace();
		}

	}

}
