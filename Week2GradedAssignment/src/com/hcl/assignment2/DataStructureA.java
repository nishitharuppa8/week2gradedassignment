package com.hcl.assignment2;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {
	public void sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> employeeNames = new ArrayList<String>();
		System.out.println("Names of all the Employees in the Sorted Order: ");
		for (Employee emp : employees) {
			employeeNames.add(emp.getName());
		}
		Collections.sort(employeeNames);
		System.out.println(employeeNames);
			
		
	}
}
